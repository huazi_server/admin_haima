import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Row, Col, Card, Form, Input, Button, message, Badge } from 'antd';
import ServiceTable from '@/component/ServiceTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';

import styles from './OrderList.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const stateFilters = [
  // 1待付款、2已取消、3已付款、4申请退款、5正在退款、6退款成功、7已关闭)
  { value: 0, text: '未完善', icon: 'default', enable: true },
  { value: 1, text: '待付款', icon: 'processing', enable: true },
  { value: 2, text: '已取消', icon: 'default', enable: true },
  { value: 3, text: '已付款', icon: 'success', enable: true },
  { value: 4, text: '申请退款', icon: 'processing', enable: true },
  { value: 5, text: '正在退款', icon: 'warning', enable: true },
  { value: 6, text: '退款成功', icon: 'error', enable: true },
  { value: 7, text: '已关闭', icon: 'default', enable: true },
];
const payTypeFilters = [
  { value: 0, text: '现金', enable: true },
  { value: 1, text: '微信', enable: true },
  { value: 2, text: '支付宝', enable: true },
];

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class OrderList extends PureComponent {
  state = {
    modalObj: {},
    modalVisible: false,
    searchFormValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalUrl = 'account/admin/student/order';

  modalDef = [
    {
      title: '订单号',
      name: 'orderNo',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '学生',
      name: 'studentName',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '课程',
      name: 'courseName',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '支付金额',
      name: 'payMoney',
      type: 'number',
      modifyDisable: true,
    },
    {
      title: '退款金额',
      name: 'refundMoney',
      type: 'number',
      step: 0.01,
    },
  ];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: text => <span>{text}</span>,
    },
    {
      title: '学生',
      dataIndex: 'studentId',
      render: (text, record) => <span>{record.studentInfo.name}</span>,
    },
    {
      title: '电话',
      dataIndex: 'phone',
      render: (text, record) => <span>{record.studentInfo.phone}</span>,
    },
    {
      title: '课程',
      dataIndex: 'courseIds',
      render: (text, record) => (
        <span>
          {record.courseType === 1 ? record.courseInfo.name : record.courseGroupInfo.name}
        </span>
      ),
    },
    {
      title: '付款方式',
      dataIndex: 'payType',
      render: val => <span>{val}</span>,
    },
    {
      title: '订单编号',
      dataIndex: 'orderNo',
      render: val => <span>{val}</span>,
    },
    {
      title: '支付时间',
      dataIndex: 'payAt',
      render: start => <span>{moment(start).format('YYYY-MM-DD HH:mm:ss')}</span>,
    },
    {
      title: '订单金额',
      dataIndex: 'orderMoney',
      render(val) {
        return <span>{val}元</span>;
      },
    },
    {
      title: '状态',
      dataIndex: 'state',
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => {
        if (record.state === 3 || record.state === 4) {
          return (
            <Fragment>
              <a onClick={() => this.handleModalUpdate(record)}>退款</a>
            </Fragment>
          );
        }
        return '';
      },
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }

  handleStateSearch = state => {
    this.searchState = state;
    this.beginSearch({ pageTotal: 0 });
  };

  beginSearch = params => {
    const { dispatch } = this.props;
    params.searchState = this.searchState;
    this.searchParams = params;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
    const { searchFormValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...searchFormValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.beginSearch(params);
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.beginSearch({ pageTotal: 0 });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleFormSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        searchFormValues: values,
      });
      this.beginSearch({ ...values, pageTotal: 0 });
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;
    if (modalObj.id === undefined) {
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/refund`,
          data: {
            orderId: modalObj.id,
            studentId: modalObj.studentId,
            refundMoney: fieldsValue.refundMoney,
          },
        },
        callback: response => {
          if (response.result) {
            message.success('退款成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error(response.error);
          }
        },
      });
    }
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    obj.studentName = obj.studentInfo.name;
    obj.courseName = obj.courseType === 1 ? obj.courseInfo.name : obj.courseGroupInfo.name;
    obj.refundMoney = obj.payMoney;
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleFormSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="手机号码">
              {getFieldDecorator('phone')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderSimpleForm();
  }

  render() {
    const { loading } = this.props;
    const { modalVisible, modalObj, searchPageList } = this.state;

    return (
      <PageHeaderWrapper title="订单管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              selectedRows={[]}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChange={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
        <ServiceModal
          title="退款"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default OrderList;
