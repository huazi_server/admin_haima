import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  InputNumber,
  DatePicker,
  message,
  Badge,
  Divider,
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceTable from '@/component/ServiceTable';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';
import ServiceFile from '@/component/ServiceFile';

import styles from './Teacher.less';

const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const stateFilters = [
  { value: 0, text: '正常', icon: 'success' },
  { value: 1, text: '禁用', icon: 'error' },
  { value: 2, text: '未激活', icon: 'warning' },
];

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class Teacher extends PureComponent {
  state = {
    modalVisible: false,
    modalObj: {},
    expandForm: false,
    selectedRows: [],
    formValues: {},
    searchTotalList: [],
    searchPageList: {
      content: [],
      pageable: {},
    },
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalUrl = 'account/admin/teacher';

  modalDef = [
    {
      title: '姓名',
      name: 'name',
      type: 'string',
    },
    {
      title: '昵称',
      name: 'nickName',
      type: 'string',
    },
    {
      title: '手机',
      name: 'phone',
      type: 'string',
    },
    {
      title: '二维码',
      name: 'qrCodeUrl',
      type: 'image',
    },
  ];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: text => <span>{text}</span>,
    },
    {
      title: '姓名',
      dataIndex: 'name',
      render: text => <span>{text}</span>,
    },
    {
      title: '电话',
      dataIndex: 'phone',
    },
    {
      title: '昵称',
      dataIndex: 'nickName',
      render: val => <span>{val}</span>,
    },
    {
      title: '头像',
      dataIndex: 'avatarUrl',
      render: (avatarUrl, record) => <ServiceFile fileType="image" fileUrl={avatarUrl} />,
    },
    {
      title: '注册时间',
      dataIndex: 'registerAt',
      render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
    },
    {
      title: '二维码',
      dataIndex: 'qrCodeUrl',
      render: (qrCodeUrl, record) => <ServiceFile fileType="image" fileUrl={qrCodeUrl} />,
    },
    {
      title: '状态',
      dataIndex: 'state',
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>编辑</a>
          <Divider type="vertical" />
          {record.state === 0 ? (
            <span>解封</span>
          ) : (
            <a onClick={() => this.handleUpdateJiefeng(record)}>解封</a>
          )}
          <Divider type="vertical" />
          {record.state === 1 ? (
            <span>封禁</span>
          ) : (
            <a onClick={() => this.handleUpdateFengjin(record)}>封禁</a>
          )}
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }

  handleStateSearch = state => {
    this.searchState = state;
    this.beginSearch({ pageTotal: 0 });
  };

  beginSearch = params => {
    const { dispatch } = this.props;
    params.searchState = this.searchState;
    this.searchParams = params;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
          dispatch({
            type: 'service/saveArray',
            payload: { type: 'teacher', data: response.data.content },
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleServiceTableSearchChange = (pagination, filters, sorter) => {
    const { formValues } = this.state;
    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      filters,
      sorter,
    };
    this.beginSearch(params);
  };

  previewItem = id => {
    router.push(`/profile/basic/${id}`);
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });

    this.beginSearch({ pageTotal: 0 });
  };

  handleFormToggle = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (selectedRows.length === 0) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'service/adminApiCall',
          payload: {
            url: `${this.modalUrl}/this/search`,
            data: {
              key: selectedRows.map(row => row.key),
            },
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleFormSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });
      this.beginSearch({ ...values, pageTotal: 0 });
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;
    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/this/create`, data: { ...fieldsValue } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { teacherId: modalObj.id, ...fieldsValue },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleUpdateJiefeng = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { teacherId: record.id, state: 0 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleUpdateFengjin = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { teacherId: record.id, state: 1 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleFormSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="手机号码">
              {getFieldDecorator('phone')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="用户姓名">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderSimpleForm();
  }

  render() {
    const { service } = this.props;
    const { loading } = this.props;
    const { selectedRows, modalVisible, modalObj, searchPageList } = this.state;
    console.log('service data');
    console.log(service);
    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );
    return (
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListForm}>{this.renderForm()}</div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={() => this.handleModalCreate()}>
              新建
            </Button>
            <ServiceButton
              stateFilters={stateFilters}
              searchState={this.searchState}
              handleStateSearch={state => this.handleStateSearch(state)}
            />
          </div>
          <ServiceTable
            loading={loading}
            data={searchPageList}
            columns={this.columns}
            selectedRows={selectedRows}
            onSelectRow={this.handleSelectRows}
            onChangeSearch={this.handleServiceTableSearchChange}
          />
        </div>
        <ServiceModal
          title="教师"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </Card>
    );
  }
}

export default Teacher;
