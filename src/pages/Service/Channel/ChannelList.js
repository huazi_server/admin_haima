import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import { Card, Form, Button, message, Badge, Divider } from 'antd';

import { arrayToString, arrayToMap } from '@/services/service';
import ServiceTable from '@/component/ServiceTable';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';

import styles from './ChannelList.less';

const stateFilters = [
  { value: 0, text: '禁用', icon: 'error' },
  { value: 1, text: '启用', icon: 'success' },
];
const stateMaps = arrayToMap(stateFilters, 'value');

@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class ChannelList extends PureComponent {
  state = {
    modalVisible: false,
    selectedRows: [],
    formValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
    modalObj: {},
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalTitle = '渠道';

  modalUrl = 'account/admin/channel';

  modalDef = [
    { title: '名字', name: 'name', type: 'string' },
    { title: '编号', name: 'number', type: 'string' },
    { title: '介绍', name: 'info', type: 'string' },
    { title: '地址', name: 'address', type: 'string' },
    { title: '描述', name: 'remark', type: 'string' },
  ];

  columns = [
    { title: 'Id', dataIndex: 'id', render: (value, record) => <span>{value}</span> },
    {
      title: '名字',
      dataIndex: 'name',
      render: (value, record) => (
        <a onClick={() => router.push(`/channel/${record.id}`)}>{value}</a>
      ),
    },
    { title: '编号', dataIndex: 'number', render: (value, record) => <span>{value}</span> },
    { title: '介绍', dataIndex: 'info', render: (value, record) => <span>{value}</span> },
    { title: '地址', dataIndex: 'address', render: (value, record) => <span>{value}</span> },
    {
      title: '备注',
      dataIndex: 'remark',
      sorter: true,
      filters: stateFilters,
      render: (value, record) => <span>{value}</span>,
    },
    {
      title: '状态',
      dataIndex: 'state',
      sorter: true,
      filters: stateFilters,
      render(value, record) {
        return stateMaps[`${value}`] ? (
          <Badge status={stateMaps[`${value}`].icon} text={stateMaps[`${value}`].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (value, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>修改</a>
          <Divider type="vertical" />
          {record.state === 0 ? (
            <a onClick={() => this.handleStateUpdate(record.id, 1)}>启用</a>
          ) : (
            <a onClick={() => this.handleStateUpdate(record.id, 0)}>禁用</a>
          )}
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }

  beginSearch = searchParams => {
    const { dispatch, match } = this.props;
    this.searchParams = searchParams;
    this.searchParams.searchState = this.searchState;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: this.searchParams },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalCancel = () => {
    this.setState({ modalVisible: false });
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch, match } = this.props;
    const params = { ...fieldsValue };
    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/this/create`, data: { ...params } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch(this.searchParams);
            this.setState({ modalVisible: false });
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { channelId: modalObj.id, ...params },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.setState({ modalVisible: false });
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleStateUpdate = (id, state) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { channelId: id, state } },
      callback: response => {
        if (response.result) {
          message.success('设置成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('设置失败');
        }
      },
    });
  };

  handleStateSearch = state => {
    this.searchState = state;
    this.searchParams.pageTotal = 0;
    this.beginSearch(this.searchParams);
  };

  handleTableSearch = (pagination, filters, sorter) => {
    const { formValues } = this.state;
    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      filters,
      sorter,
    };
    this.beginSearch(params);
  };

  handleSelectRows = rows => {
    const { onSelect } = this.props;
    this.setState({
      selectedRows: rows,
    });
    if (onSelect) {
      onSelect(rows);
    }
  };

  render() {
    const { loading } = this.props;
    const { selectedRows, modalVisible, modalObj, searchPageList } = this.state;

    return (
      <Card bordered={false}>
        <div className={styles.tableList}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={() => this.handleModalCreate()}>
              新建
            </Button>
            <ServiceButton
              stateFilters={stateFilters}
              searchState={this.searchState}
              handleStateSearch={state => this.handleStateSearch(state)}
            />
          </div>
          <ServiceTable
            rowKey="id"
            selectedRows={selectedRows}
            loading={loading}
            data={searchPageList}
            columns={this.columns}
            onSelectRow={this.handleSelectRows}
            onChangeSearch={this.handleTableSearch}
          />
        </div>
        <ServiceModal
          title={this.modalTitle}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
          modalObj={modalObj}
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
        />
      </Card>
    );
  }
}

export default ChannelList;
