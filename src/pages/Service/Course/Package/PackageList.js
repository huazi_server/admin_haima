import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Card, Button, Icon, List, message } from 'antd';

import Ellipsis from '@/components/Ellipsis';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceModal from '@/component/ServiceModal';

import router from 'umi/router';
import styles from './PackageList.less';

@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
class PackageList extends PureComponent {
  state = {
    modalVisible: false,
    modalObj: {},
    searchTotalList: [],
    searchPageList: {
      content: [],
      pageable: {},
    },
  };

  // @ts-ignore
  searchParams = {};

  modalUrl = 'account/admin/coursepackage';

  modalDef = [
    {
      title: '名称',
      name: 'name',
      type: 'string',
    },
    {
      title: '课时数',
      name: 'lessonCount',
      type: 'integer',
    },
    {
      title: '封面',
      name: 'coverUrl',
      type: 'image',
    },
    {
      title: '描述',
      name: 'remark',
      type: 'text',
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0, pageSize: 100 });
  }

  beginSearch = params => {
    const { dispatch } = this.props;
    this.searchParams = params;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
            searchTotalList: [...response.data.content],
          });
        } else {
          this.setState({
            searchPageList: {},
            searchTotalList: [],
          });
        }
      },
    });
  };

  beginSearchMore = () => {
    const { dispatch } = this.props;
    const { searchTotalList, searchPageList } = this.state;
    this.searchParams.pageNumber = searchPageList.pageable.pageNumber + 1;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: this.searchParams },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
            searchTotalList: [...searchTotalList, ...response.data.content],
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;
    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/this/create`, data: { ...fieldsValue } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { packageId: modalObj.id, ...fieldsValue },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleModalUpdate = obj => {
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleModalMore = item => {
    router.push(`/package/${item.id}`);
  };

  render() {
    const { loading } = this.props;
    const { searchTotalList, modalVisible, modalObj, searchPageList } = this.state;
    let list = ['', ...searchTotalList];
    list = [...searchTotalList];

    const loadMore =
      searchPageList != null &&
      searchPageList.content !== null &&
      searchPageList.content.length > 0 ? (
        <div style={{ textAlign: 'center', marginTop: 16 }}>
          <Button onClick={this.beginSearchMore} style={{ paddingLeft: 48, paddingRight: 48 }}>
            {loading ? (
              <span>
                <Icon type="loading" /> 加载中...
              </span>
            ) : (
              '加载更多'
            )}
          </Button>
        </div>
      ) : (
        <div style={{ textAlign: 'center', marginTop: 16 }}>
          <span>暂无更多课程包</span>{' '}
        </div>
      );
    return (
      <PageHeaderWrapper title="课程包">
        <div className={styles.cardList}>
          <List
            rowKey="id"
            loading={loading}
            grid={{ gutter: 24, lg: 3, md: 2, sm: 1, xs: 1 }}
            dataSource={list}
            loadMore={loadMore}
            renderItem={item =>
              item ? (
                <List.Item key={item.id}>
                  <Card
                    hoverable
                    className={styles.card}
                    actions={[
                      <a onClick={() => this.handleModalUpdate(item)}>修改信息</a>,
                      <a onClick={() => this.handleModalMore(item)}>编辑课时</a>,
                    ]}
                  >
                    <Card.Meta
                      avatar={<img alt="" className={styles.cardAvatar} src={item.coverUrl} />}
                      title={
                        <a>
                          [LV{item.stage}] {item.name} - 共 {item.lessonCount} 课时
                        </a>
                      }
                      description={
                        <Ellipsis className={styles.item} lines={3}>
                          {item.remark}
                        </Ellipsis>
                      }
                    />
                  </Card>
                </List.Item>
              ) : (
                <List.Item key="-1">
                  <Button
                    type="dashed"
                    onClick={() => this.handleModalCreate()}
                    className={styles.newButton}
                  >
                    <Icon type="plus" /> 新建课程包
                  </Button>
                </List.Item>
              )
            }
          />
        </div>
        <ServiceModal
          title="课程包"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default PackageList;
