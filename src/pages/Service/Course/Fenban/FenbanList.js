import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import { Row, Col, Card, Form, Select, Button, message, Badge, Divider, Tag } from 'antd';
import ServiceTable from '@/component/ServiceTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';

import styles from './FenbanList.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const stateFilters = [
  { value: 0, text: '未完善', icon: 'success' },
  { value: 1, text: '待上线', icon: 'success' },
  { value: 2, text: '已上线', icon: 'success' },
  { value: 3, text: '报名中', icon: 'success' },
  { value: 4, text: '报名截止', icon: 'success' },
  { value: 5, text: '课程进行中', icon: 'success' },
  { value: 6, text: '课程结束', icon: 'success' },
];
const courseTypeFilters = [
  { value: 0, text: '正常' },
  { value: 1, text: '试听课' },
  { value: 2, text: '线上课' },
  { value: 3, text: '线下课' },
];

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class FenbanList extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    packageList: [],
    teacherList: [],
    searchPageList: {
      content: [],
      pageable: {},
    },
    modalObj: {},
    loading: false,
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalUrl = 'account/admin/course';

  modalDef = [
    {
      title: '名字',
      name: 'name',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '教师',
      name: 'teacherIds',
      type: 'selectMuti',
      list: [],
    },
  ];

  ModalTeacherIdsItem = this.modalDef[1];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '名称',
      dataIndex: 'name',
      render: (text, record) => (
        <span>
          [{record.coursePackageInfo.name}]
          <br />
          {text}
        </span>
      ),
    },
    {
      title: '信息',
      dataIndex: 'coursePackageId',
      render: (text, record) => (
        <span>
          {record.issue}期
          {courseTypeFilters[record.courseType] ? courseTypeFilters[record.courseType].text : ''}
          <br />
          招生人数:
          {record.enrollCount === null || record.enrollCount === 0 ? '不限' : record.enrollCount}
        </span>
      ),
    },
    {
      title: '总人数',
      dataIndex: 'totalCount',
      render: (text, record) => <span>{record.totalCount ? record.totalCount : 0}</span>,
    },
    {
      title: '待分班',
      dataIndex: 'settleCount',
      render: (text, record) => <span>{record.settleCount ? record.settleCount : 0}</span>,
    },
    {
      title: '授课教师',
      dataIndex: 'planTeacherIds',
      render: (text, record) =>
        record.planTeacherIds
          ? record.planTeacherIds.split(',').map(teacherId => {
              let aaateacher = null;
              this.state.teacherList.forEach(teacher => {
                if (`${teacher.id}` === teacherId) {
                  aaateacher = teacher;
                }
              });
              return aaateacher ? <Tag key={aaateacher.id}>{aaateacher.name}</Tag> : null;
            })
          : '',
    },
    {
      title: '状态',
      dataIndex: 'state',
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>分配老师</a>
          <Divider type="vertical" />
          <a onClick={() => this.fenpeistudent(record.id)}>分配学生</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
    this.beginSearchPackage({ pageSize: 100, state: 1 });
    this.beginSearchTeacher({ pageSize: 100, state: 0 });
  }

  beginSearchTeacher = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: 'account/admin/teacher/this/search', data: params },
      callback: response => {
        if (response.result) {
          this.ModalTeacherIdsItem.list = [...response.data.content];
          this.setState({ teacherList: [...response.data.content] });
        } else {
          this.setState({ teacherList: [] });
        }
      },
    });
  };

  beginSearchPackage = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `account/admin/coursepackage/this/search`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({ packageList: [...response.data.content] });
        } else {
          this.setState({ packageList: [] });
        }
      },
    });
  };

  handleStateSearch = state => {
    this.searchState = state;
    this.beginSearch({ pageTotal: 0 });
  };

  beginSearch = params => {
    this.setState({ loading: true });
    const { dispatch } = this.props;
    params.searchState = this.searchState;
    params.action = 'count';
    this.searchParams = params;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: params },
      callback: response => {
        this.setState({ loading: false });
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.beginSearch(params);
  };

  fenpeistudent = id => {
    const { match } = this.props;
    router.push(`/fenban/${id}/student`);
  };

  fenpeiteacher = id => {
    const { match } = this.props;
    router.push(`/fenban/${id}/teacher`);
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });

    this.beginSearch({ pageTotal: 0 });
  };

  handleFormToggle = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (selectedRows.length === 0) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'service/adminApiCall',
          payload: {
            url: `${this.modalUrl}/this/search`,
            data: {
              key: selectedRows.map(row => row.key),
            },
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleFormSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });
      this.beginSearch({ ...values, pageTotal: 0 });
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;

    const params = { ...fieldsValue };
    params.planTeacherIds = fieldsValue.teacherIds.join();

    if (modalObj.id === undefined) {
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { courseId: modalObj.id, ...params },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleUpdateJiefeng = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { teacherId: record.id, state: 0 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleUpdateFengjin = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { teacherId: record.id, state: 1 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    this.ModalTeacherIdsItem.values = obj.planTeacherIds ? obj.planTeacherIds.split(',') : [];
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };
    });
  };

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { packageList, teacherList } = this.state;
    return (
      <Form onSubmit={this.handleFormSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="课程包">
              {getFieldDecorator('coursePackageId')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {packageList.map(item => (
                    <Option key={item.id}>{item.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="教师">
              {getFieldDecorator('planTeacherIds')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {teacherList.map(item => (
                    <Option key={item.id}>{item.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <div style={{ overflow: 'hidden' }}>
              <div style={{ marginBottom: 24 }}>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderAdvancedForm();
  }

  render() {
    const { loading } = this.state;
    const { selectedRows, modalVisible, modalObj, searchPageList } = this.state;

    return (
      <PageHeaderWrapper title="课程分班">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              selectedRows={selectedRows}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChange={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
        <ServiceModal
          title="课程"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default FenbanList;
