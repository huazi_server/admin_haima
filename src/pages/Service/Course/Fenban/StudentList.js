import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import { Form, Card, Badge, Divider, Table, message, Tag, Button, Dropdown, Icon } from 'antd';
import ServiceModal from '@/component/ServiceModal';
import ServiceTable from '@/component/ServiceTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import DescriptionList from '@/components/DescriptionList';
const { Description } = DescriptionList;

import moment from 'moment';
import router from 'umi/router';
import styles from './StudentList.less';

const stateFilters = [
  { value: 0, text: '正常', icon: 'success' },
  { value: 1, text: '未分配老师', icon: 'error' },
  { value: 2, text: '已分配老师', icon: 'success' },
];
const sourceTypeFilters = [{ value: 0, text: '线上' }, { value: 1, text: '线下' }];

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class StudentList extends Component {
  state = {
    modalVisible: false,
    selectedRows: [],
    modalObj: {},
    modalDetail: { coursePackageInfo: {}, teacherInfoList: [] },
    searchPageList: {
      content: [],
      pageable: {},
    },
  };

  // @ts-ignore
  searchParams = {};

  modalUrl = 'account/admin/course/student';

  courseUrl = 'account/admin/course';

  modalDef = [
    {
      title: '姓名',
      name: 'name',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '老师',
      name: 'teacherId',
      type: 'selectSingle',
      list: [],
    },
  ];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: text => <span>{text}</span>,
    },
    {
      title: '姓名',
      dataIndex: 'name',
      render: (text, record) => <span>{record.studentInfo.name}</span>,
    },
    {
      title: '电话',
      dataIndex: 'phone',
      render: (text, record) => <span>{record.studentInfo.phone}</span>,
    },
    {
      title: '分配老师',
      dataIndex: 'teacher',
      render: (text, record) => (
        <a onClick={() => this.previewItem(text)}>
          {record.teacherInfo ? record.teacherInfo.name : ''}
        </a>
      ),
    },
    {
      title: '状态',
      dataIndex: 'state',
      filters: stateFilters,
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>设置老师</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
    this.getCourseDeatil({ pageSize: 100, state: 0 });
  }

  getCourseDeatil = () => {
    const { dispatch, match } = this.props;
    const { params } = match;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.courseUrl}/this/detail`, data: { courseId: params.id } },
      callback: response => {
        if (response.result) {
          this.modalDef[1].list = [...response.data.teacherInfoList];
          this.setState({
            modalDetail: response.data,
          });
        } else {
          this.modalDef[1].list = [];
          this.setState({
            modalDetail: {},
          });
        }
      },
    });
  };

  beginSearch = params => {
    const { dispatch, match } = this.props;
    this.searchParams = params;
    dispatch({
      type: 'service/adminApiCall',
      payload: {
        url: `${this.modalUrl}/this/search`,
        data: { ...params, courseId: match.params.id },
      },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  previewItem = id => {
    const { match } = this.props;
    router.push(`${match.url}/${id}`);
  };

  handleUpdateJiefeng = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { studentId: record.id, state: 0 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleUpdateFengjin = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { studentId: record.id, state: 1 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { studentId, courseId } = modalObj;
    const { dispatch } = this.props;
    if (modalObj.id === undefined) {
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/settle`, data: { ...fieldsValue, studentId, courseId } },
        callback: response => {
          if (response.result) {
            message.success('设置成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('设置失败');
          }
        },
      });
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.beginSearch(params);
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    obj.name = obj.studentInfo.name;
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  render() {
    const { loading } = this.props;
    const { selectedRows, modalVisible, modalObj, searchPageList, modalDetail } = this.state;

    const mainSearch = (
      <DescriptionList size="large" title="" style={{ marginBottom: 32 }}>
        <Description term="名称">{modalDetail.name}</Description>
        <Description term="课程包">{modalDetail.coursePackageInfo.name}</Description>
        <Description term="老师">
          {modalDetail.teacherInfoList
            ? modalDetail.teacherInfoList.map(teacher => <Tag key={teacher.id}>{teacher.name}</Tag>)
            : ''}
        </Description>
        <Description term="报名时间">
          {moment(modalDetail.signStartAt).format('YYYY-MM-DD')} 至{' '}
          {moment(modalDetail.signEndAt).format('YYYY-MM-DD')}
        </Description>
        <Description term="上课时间">
          {moment(modalDetail.teachStartAt).format('YYYY-MM-DD')} 至{' '}
          {moment(modalDetail.teachEndAt).format('YYYY-MM-DD')}
        </Description>
      </DescriptionList>
    );

    return (
      <PageHeaderWrapper title="课程信息" content={mainSearch}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <span>
                <Button>批量设置老师</Button>
              </span>
            </div>
            <ServiceTable
              selectedRows={selectedRows}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
        <ServiceModal
          title="学生"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default StudentList;
