import { query as queryUsers } from '@/services/user';
import { adminApi } from '@/services/service';

export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryUsers);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *fetchCurrent(_, { call, put }) {
      const url = 'account/admin/info';
      const data = {};
      const response = yield call(adminApi, { url, data });
      if (response.code === 402) {
        window.g_app._store.dispatch({
          type: 'login/logout',
        });
      } else if (response.result === true) {
        const user = {};
        user.avatar = response.data.avatarUrl;
        user.name = response.data.name;
        user.phone = response.data.phone;
        user.email = response.data.email;
        yield put({
          type: 'saveCurrentUser',
          payload: user,
        });
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};
