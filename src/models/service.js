import { serviceApi, adminApi } from '@/services/service';

export default {
  namespace: 'service',
  state: {},
  effects: {
    *adminApiCall({ payload, callback }, { call }) {
      const { url, data } = payload;
      const response = yield call(adminApi, { url, data });
      if (response.code === 402) {
        window.g_app._store.dispatch({
          type: 'login/logout',
        });
      } else if (callback) callback(response);
    },
    *serviceApiCall({ payload, callback }, { call }) {
      const { url, data } = payload;
      const response = yield call(serviceApi, { url, data });
      if (response.code === 402) {
        window.g_app._store.dispatch({
          type: 'login/logout',
        });
      } else if (callback) callback(response);
    },
  },
  reducers: {
    saveArray(state, action) {
      console.log('saveArray');
      const amap = arrayToMap(action.payload.data, 'id');
      console.log(amap);
      const newstate = { ...state };
      newstate[action.payload.type] = { ...state[action.payload.type], ...amap };
      return newstate;
    },
    saveData(state, action) {
      const newstate = { ...state };
      newstate[action.payload.type] = { ...state[action.payload.type], ...action.payload.data };
      return newstate;
    },
  },
};
