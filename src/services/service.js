import request from '@/utils/request';
import { message } from 'antd';

export function arrayToString(obj, char) {
  return Object.keys(obj)
    .map(key => obj[key])
    .join(char);
}

export function arrayToMap(array, name) {
  console.log('arrayToMap');
  const amap = {};
  array.map(item => (amap[item[name]] = item));
  return amap;
}

export async function serviceApi(params) {
  const { data, url } = params;
  data.loginToken = localStorage.getItem('loginToken');
  let type = 0;
  if (window.location.href.indexOf('localhost') > 0) type = 0;
  if (window.location.href.startsWith('https://www.haimacode.com/test')) type = 1;
  if (window.location.href.startsWith('https://www.haimacode.com/admin')) type = 2;

  let adminurl = `https://www.haimacode.com/api/service/`;
  if (type === 0) adminurl = `http://localhost:8280/HuaziServiceApi/`;
  if (type === 1) adminurl = `https://www.haimacode.com/test/api/service/`;
  if (type === 2) adminurl = `https://www.haimacode.com/api/service/`;
  adminurl = `https://www.haimacode.com/api/service/`;

  return request(`${adminurl}${url}`, {
    method: 'POST',
    data,
  }).then(response => {
    if (type !== 2) {
      console.log('*******************');
      console.log(params);
      console.log(response);
    }
    return response;
  });
}

export async function adminApi(params) {
  const { data, url } = params;
  data.loginToken = localStorage.getItem('loginToken');
  let type = 0;
  if (window.location.href.indexOf('localhost') > 0) type = 1;
  if (window.location.href.startsWith('https://www.haimacode.com/test')) type = 1;
  if (window.location.href.startsWith('https://www.haimacode.com/admin')) type = 2;
  let adminurl = `https://www.haimacode.com/api/haimaadmin/`;
  if (type === 2) adminurl = `https://www.haimacode.com/api/haimaadmin/`;
  if (type === 1) adminurl = `https://www.haimacode.com/test/api/haimaadmin/`;
  if (type === 0) adminurl = `http://localhost:8180/HaimaAdminApi/`;
  return request(`${adminurl}${url}`, {
    method: 'POST',
    data,
  }).then(response => {
    if (type !== 2) {
      console.log('*******************');
      console.log(params);
      console.log(response);
    }
    return response;
  });
}
