import React, { PureComponent, Fragment } from 'react';
import { Table, Alert } from 'antd';
import { arrayToString, arrayToMap } from '@/services/service';
import styles from './index.less';

function initTotalList(columns) {
  const totalList = [];
  columns.forEach(column => {
    if (column.needTotal) {
      totalList.push({ ...column, total: 0 });
    }
  });
  return totalList;
}

class ServiceTable extends PureComponent {
  constructor(props) {
    super(props);
    const { columns } = props;
    const needTotalList = initTotalList(columns);

    this.state = {
      selectedRowKeys: [],
      needTotalList,
    };
  }

  static getDerivedStateFromProps(nextProps) {
    // clean state
    if (nextProps.selectedRows.length === 0) {
      const needTotalList = initTotalList(nextProps.columns);
      return {
        selectedRowKeys: [],
        needTotalList,
      };
    }
    return null;
  }

  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    let { needTotalList } = this.state;
    needTotalList = needTotalList.map(item => ({
      ...item,
      total: selectedRows.reduce((sum, val) => sum + parseFloat(val[item.dataIndex], 10), 0),
    }));
    const { onSelectRow } = this.props;
    if (onSelectRow) {
      onSelectRow(selectedRows);
    }

    this.setState({ selectedRowKeys, needTotalList });
  };

  handleTableChange = (pagination, filters, sorter) => {
    const { onChangeSearch } = this.props;
    if (onChangeSearch) {
      const filtersArray = [];
      const filtersKeys = Object.keys(filters).reduce((obj, key) => {
        const newObj = { ...obj };
        newObj[key] = `${key}:${arrayToString(filters[key], ',')}`;
        filtersArray.push(newObj[key]);
        return newObj;
      }, {});
      const filterstring = filtersArray.join(';');
      let sorterstring = '';
      if (sorter.field) {
        sorterstring = `${sorter.field}_${sorter.order}`;
      }
      onChangeSearch(pagination, filterstring, sorterstring);
    }
  };

  cleanSelectedKeys = () => {
    this.handleRowSelectChange([], []);
  };

  render() {
    const { selectedRowKeys, needTotalList } = this.state;
    const {
      data = {},
      rowKey,
      chooseKey,
      chooseType,
      selectedRows,
      onSelectRow,
      onChange,
      ...rest
    } = this.props;
    const { content = [], pageable } = data;
    let current = 1;
    let pageSize = 0;
    let total = 0;
    if (pageable !== undefined) {
      current = pageable.pageNumber === undefined ? 1 : pageable.pageNumber + 1;
      total = pageable.pageTotal === undefined ? content.length : pageable.pageTotal;
      pageSize = pageable.pageSize;
    }
    const pagination = { current, total, pageSize, position: 'bottom' };
    // pagination = pageable;
    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      ...pagination,
    };

    let rowSelection = {
      selectedRowKeys,
      onChange: this.handleRowSelectChange,
      getCheckboxProps: record => ({
        disabled: record.disabled,
      }),
      type: chooseType || 'checkbox',
    };

    let alert = (
      <Alert
        message={
          <Fragment>
            已选择 <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> 项&nbsp;&nbsp;
            {needTotalList.map(item => (
              <span style={{ marginLeft: 8 }} key={item.dataIndex}>
                {item.title}
                总计&nbsp;
                <span style={{ fontWeight: 600 }}>
                  {item.render ? item.render(item.total) : item.total}
                </span>
              </span>
            ))}
            <a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>
              清空
            </a>
          </Fragment>
        }
        type="info"
        showIcon
      />
    );
    if (!onSelectRow) {
      rowSelection = null;
      alert = null;
    }

    return (
      <div className={styles.standardTable}>
        <div className={styles.tableAlert}>{alert}</div>
        <Table
          rowKey={rowKey || 'id'}
          rowSelection={rowSelection}
          dataSource={content}
          pagination={paginationProps}
          onChange={this.handleTableChange}
          {...rest}
        />
      </div>
    );
  }
}

export default ServiceTable;
