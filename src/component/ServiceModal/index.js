import React, { Component } from 'react';
import {
  Button,
  Col,
  Form,
  Icon,
  Input,
  InputNumber,
  message,
  Modal,
  Row,
  Select,
  DatePicker,
} from 'antd';
import moment from 'moment';
import styles from './index.less';
import ServiceUpload from '@/component/ServiceUpload';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const { TextArea } = Input;
const { Option } = Select;

@Form.create()
class ServiceModal extends Component {
  okHandle = () => {
    const { modalObj, form, handleOK } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      handleOK({ modalObj, fieldsValue });
    });
  };

  cancelHandle = () => {
    const { handleCancel } = this.props;
    handleCancel();
  };

  handleFilesChange = changeValue => {
    const { form } = this.props;
    const { name, url } = changeValue;
    console.log(name, url);
    const a = {};
    a[name] = url;
    form.setFieldsValue({
      ...a,
    });
  };

  render() {
    const { modalVisible, modalObj, modalDef, form, title } = this.props;
    const elements = [];
    modalDef.forEach((item, index) => {
      if (item.type === 'string') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            {form.getFieldDecorator(item.name, {
              initialValue:
                modalObj[item.name] === undefined || modalObj[item.name] === ''
                  ? ''
                  : modalObj[item.name] + '',
              rules: [
                {
                  required: item.required === undefined ? true : item.required,
                  message: `请输入${item.title}！`,
                  min: 1,
                },
              ],
            })(
              <Input
                placeholder="请输入"
                disabled={!!(modalObj.id && item.modifyDisable === true)}
              />
            )}
          </FormItem>
        );
      } else if (item.type === 'rangedatetime') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            {form.getFieldDecorator(item.name, {
              initialValue: [
                item.times[0] ? moment(item.times[0]) : '',
                item.times[1] ? moment(item.times[1]) : '',
              ],
              rules: [
                {
                  required: item.required === undefined ? true : item.required,
                  message: `请输入${item.title}！`,
                },
              ],
            })(
              <RangePicker
                showTime={{ format: 'HH:mm:ss' }}
                format="YYYY-MM-DD HH:mm:ss"
                placeholder={['Start Time', 'End Time']}
                disabled={!!(modalObj.id && item.modifyDisable === true)}
              />
            )}
          </FormItem>
        );
      } else if (item.type === 'datetime') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            {form.getFieldDecorator(item.name, {
              initialValue: modalObj[item.name] ? moment(modalObj[item.name]) : '',
              rules: [
                {
                  required: item.required === undefined ? true : item.required,
                  message: `请输入${item.title}！`,
                },
              ],
            })(
              <DatePicker
                showTime
                format="YYYY-MM-DD HH:mm:ss"
                placeholder="请输入"
                disabled={!!(modalObj.id && item.modifyDisable === true)}
              />
            )}
          </FormItem>
        );
      } else if (item.type === 'integer' || item.type === 'number') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            {form.getFieldDecorator(item.name, {
              initialValue: modalObj[item.name],
              rules: [
                {
                  required: item.required === undefined ? true : item.required,
                  message: `请输入${item.title}！`,
                },
              ],
            })(
              <InputNumber
                step={item.step}
                placeholder="请输入"
                disabled={!!(modalObj.id && item.modifyDisable === true)}
              />
            )}
          </FormItem>
        );
      } else if (item.type === 'text') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            {form.getFieldDecorator(item.name, {
              initialValue: modalObj[item.name],
              rules: [
                {
                  required: item.required === undefined ? true : item.required,
                  message: `请输入${item.title}！`,
                  min: 2,
                },
              ],
            })(
              <TextArea
                placeholder="请输入"
                disabled={!!(modalObj.id && item.modifyDisable === true)}
              />
            )}
          </FormItem>
        );
      } else if (item.type === 'selectSingle') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            {form.getFieldDecorator(item.name, {
              initialValue: modalObj[item.name] ? `${modalObj[item.name]}` : [],
              rules: [
                {
                  required: item.required === undefined ? true : item.required,
                  message: `请输入${item.title}！`,
                },
              ],
            })(
              <Select
                placeholder="请选择"
                style={{ width: '100%' }}
                disabled={!!(modalObj.id && item.modifyDisable === true)}
              >
                {item.list.map(aitem => (
                  <Option key={aitem.id ? aitem.id : aitem.value}>
                    {aitem.name ? aitem.name : aitem.text ? aitem.text : aitem.title}
                  </Option>
                ))}
              </Select>
            )}
          </FormItem>
        );
      } else if (item.type === 'selectMuti') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            {form.getFieldDecorator(item.name, {
              initialValue: item.values,
              rules: [
                {
                  required: item.required === undefined ? true : item.required,
                  message: `请输入${item.title}！`,
                },
              ],
            })(
              <Select
                mode="multiple"
                placeholder="请选择"
                style={{ width: '100%' }}
                disabled={!!(modalObj.id && item.modifyDisable === true)}
              >
                {item.list.map(aitem => (
                  <Option key={aitem.id ? aitem.id : aitem.value}>
                    {aitem.name ? aitem.name : aitem.text ? aitem.text : aitem.title}
                  </Option>
                ))}
              </Select>
            )}
          </FormItem>
        );
      } else if (item.type === 'image') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            <Row>
              <ServiceUpload
                fileType="image"
                name={item.name}
                handleFilesChange={this.handleFilesChange}
                fileUrl={modalObj[item.name]}
              />
            </Row>
            <Row>
              {form.getFieldDecorator(item.name, {
                initialValue: modalObj[item.name],
                rules: [
                  {
                    required: item.required === undefined ? true : item.required,
                    message: `请输入${item.title}！`,
                  },
                ],
              })(<Input placeholder="请输入" disabled />)}
            </Row>
          </FormItem>
        );
      } else if (item.type === 'file') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            <Row>
              <ServiceUpload
                fileType="file"
                name={item.name}
                handleFilesChange={this.handleFilesChange}
                fileUrl={modalObj[item.name]}
              />
            </Row>
            <Row>
              {form.getFieldDecorator(item.name, {
                initialValue: modalObj[item.name],
                rules: [
                  {
                    required: item.required === undefined ? true : item.required,
                    message: `请输入${item.title}！`,
                  },
                ],
              })(<Input placeholder="请输入" disabled />)}
            </Row>
          </FormItem>
        );
      } else if (item.type === 'video') {
        elements.push(
          <FormItem
            key={item.name}
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 15 }}
            label={item.title}
          >
            <Row>
              <ServiceUpload
                fileType="video"
                name={item.name}
                handleFilesChange={this.handleFilesChange}
                fileUrl={modalObj[item.name]}
              />
            </Row>
            <Row>
              {form.getFieldDecorator(item.name, {
                initialValue: modalObj[item.name],
                rules: [
                  {
                    required: item.required === undefined ? true : item.required,
                    message: `请输入${item.title}！`,
                  },
                ],
              })(<Input placeholder="请输入" disabled />)}
            </Row>
          </FormItem>
        );
      }
    });

    return (
      <Modal
        destroyOnClose
        title={title}
        visible={modalVisible}
        onOk={this.okHandle}
        onCancel={this.cancelHandle}
      >
        {elements}
      </Modal>
    );
  }
}

export default ServiceModal;
