import React, { Component, Fragment, PureComponent } from 'react';
import { Modal, Carousel, Avatar } from 'antd';
import styles from './index.less';

class ServiceFile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showVisible: false,
      showIndex: 0,
    };
  }

  stateSearch = keyindex => {
    this.setState({
      showVisible: true,
      showIndex: keyindex,
    });
  };

  cancelHandle = () => {
    this.setState({
      showVisible: false,
    });
  };

  render() {
    const { fileType, fileUrl } = this.props;
    const { showVisible, showIndex } = this.state;

    const showButtons = [];
    const carousels = [];
    let title = '查看';
    const files = fileUrl ? fileUrl.split(',') : [];
    if (files && files.length > 0) {
      files.forEach((item, index) => {
        const keyindex = index + 1;
        if (fileType === 'image') {
          title = '查看图片';
          if (index === 0) {
            showButtons.push(
              <Avatar
                className={styles.tableListStateBG}
                size={50}
                src={item}
                shape="square"
                key={keyindex}
                onClick={() => this.stateSearch(keyindex)}
              />
            );
          }
          if (index === 1) {
            showButtons.push(
              <Avatar
                className={styles.tableListStateBG}
                size={50}
                shape="square"
                key={keyindex}
                onClick={() => this.stateSearch(keyindex)}
              >{`${files.length}`}</Avatar>
            );
          }

          carousels.push(
            <div key={keyindex}>
              <img width="100%" src={item} />
            </div>
          );
        } else if (fileType === 'video') {
          title = '查看视频';
          if (index === 0) {
            showButtons.push(
              <Avatar
                className={styles.tableListStateBG}
                size={50}
                shape="square"
                icon="caret-right"
                key={keyindex}
                onClick={() => this.stateSearch(keyindex)}
              />
            );
          }
          if (index === 1) {
            showButtons.push(
              <Avatar
                className={styles.tableListStateBG}
                size={50}
                shape="square"
                key={keyindex}
                onClick={() => this.stateSearch(keyindex)}
              >{`${files.length}`}</Avatar>
            );
          }

          carousels.push(
            <video src={item} width="100%" key={keyindex} controls="controls">
              您的浏览器不支持 video 标签。
            </video>
          );
        } else if (fileType === 'file') {
          title = '查看文件';
          if (index === 0) {
            showButtons.push(
              <Avatar
                className={styles.tableListStateBG}
                size={50}
                shape="square"
                icon="caret-right"
                key={keyindex}
                onClick={() => this.stateSearch(keyindex)}
              />
            );
          }
          if (index === 1) {
            showButtons.push(
              <Avatar
                className={styles.tableListStateBG}
                size={50}
                shape="square"
                key={keyindex}
                onClick={() => this.stateSearch(keyindex)}
              >{`${files.length}`}</Avatar>
            );
          }

          carousels.push(
            <video src={item} width="100%" key={keyindex} controls="controls">
              您的浏览器不支持 video 标签。
            </video>
          );
        }
      });
    }

    return (
      <Fragment>
        {showButtons}
        <Modal title={title} visible={showVisible} onCancel={this.cancelHandle} footer={null}>
          <Carousel>{carousels}</Carousel>
        </Modal>
      </Fragment>
    );
  }
}

export default ServiceFile;
